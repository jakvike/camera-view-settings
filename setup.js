import { CameraViewSettings } from './scripts/settings.js';
import { CameraView } from './scripts/cameraView.js';

Hooks.on('renderAVConfig', async (app, html, user) => {
    CameraView.onAvRender(app, html, user);
}); 

Hooks.once('init', async function() {
    CameraViewSettings.registerSettings();
});

Hooks.once('ready', async () => {
    CameraViewSettings.registerUserSettings();
    CameraViewSettings.registerLocalizedSettings();
});

Hooks.on('closeAVConfig', async (app, html) => {
    CameraView.onAvClose(html);
});

Hooks.on('renderCameraViews', async (cameraViews, html, user) => {
    CameraView.onAvViewRender(html);    
});