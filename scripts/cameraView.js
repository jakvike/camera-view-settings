export class CameraView {

    static async onAvRender(app, html){
        let cameraViewSettings = CONFIG.cameraViewSettings;
        const tab = app._tabs[0].active;
        
        if(tab && tab === 'general'){            
            cameraViewSettings.currentSettings.cameraView = game.user.getFlag("camera-view-settings", "viewSetting");
            const template = await renderTemplate(`${cameraViewSettings.templatesPath}/camera-view-form.html`, cameraViewSettings);
            const element = html.find("div[data-tab='general']");
            if(element){
                element[0].insertAdjacentHTML('afterend', template);
            }        
        }
    }

    static onAvClose(html){
        const cameraViewElement = html.find("select[id='cameraViews'");
        let selectedValue = cameraViewElement[0].selectedOptions[0].value;
        let config = CONFIG.cameraViewSettings;
        let currentConfigCameraView = config.currentSettings;

        if(selectedValue && currentConfigCameraView.cameraView !== selectedValue){
            currentConfigCameraView.cameraView = selectedValue;
            game.user.setFlag("camera-view-settings", "viewSetting", selectedValue);
        }
    }

    static onAvViewRender(){
        let currentValue = game.user.getFlag("camera-view-settings", "viewSetting");
        this._setView(currentValue);        
    }

    static _setView(view){
        $("#camera-views").show();
        $(".video-container ").show();
        $(".notification-bar").show();
        
        switch(view) {
            case "Hide":
                $("#camera-views").hide();
                $(".video-container ").hide();
                break;
            case "Names Only":
                $(".video-container ").hide();
                $(".notification-bar").hide();
                break;
            default:
                break;
        }
    }
}