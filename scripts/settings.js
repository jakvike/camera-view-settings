
export class CameraViewSettings {

	static registerSettings(){
		if(!CONFIG.cameraViewSettings){
			let camerViewConfig = {				
				currentSettings: {
					cameraView: "Full"
				},
				templatesPath: "/modules/camera-view-settings/templates"			
			};
			CONFIG.cameraViewSettings = camerViewConfig;
		}
	}

	static registerUserSettings(){		
		let userViewSettings = game.user.getFlag("camera-view-settings", "viewSetting");
		if(!userViewSettings)
		{            
			game.user.setFlag("camera-view-settings", "viewSetting", "Full");
		}
	}

	static registerLocalizedSettings(){
		CONFIG.cameraViewSettings.localized = {
			cameraViewLabel : game.i18n.localize("CAMERAVIEW.CameraViewLabel"),	
			full: game.i18n.localize("CAMERAVIEW.Full"),
			hide: game.i18n.localize("CAMERAVIEW.Hide"),
			namesOnly: game.i18n.localize("CAMERAVIEW.NamesOnly"),
			fullDesc: game.i18n.localize("CAMERAVIEW.FullDesc"),
			hideDesc: game.i18n.localize("CAMERAVIEW.HideDesc"),
			namesOnlyDesc: game.i18n.localize("CAMERAVIEW.NamesOnlyDesc"),		
		};
	}
}
