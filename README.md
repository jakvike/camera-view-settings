The intent of this module is to give the user the ability to change the camera view.
The user has three options: 
- Hide Hides everything within the Camera View.  (nice if you have an observer user like HotSeat Observer)
- Names Only This will only show the names. Useful especially if you only use Audio. 
- Full (default) This is the default view. Both Camera and names are viewable.


Settings are appended within the Audio/Video Configuration dialog, within Core Settings.
![image_1.png](./image_1.png)
